# Météo

Ce projet permet d'afficher des informations météo sur un lieu donné

# Installation

- Clônez le dépot sur votre serveur Web
- Créez une base Mysql et importez sa structure avec le fichier /sql/init_base_meteo.sql
- Importez la table paramètre pour les icones de météo : /sql/init_icones_weathermap.sql
- Modifiez le fichier de configuration /includes/config.inc.php.exemple pour y mettre vos données et renommez-le en config.inc.php
- Créez une tâche cron pour exécuter régulièrement le script /capture_weathermap.php (toutes les 15 minutes par exemple) : récupère les données de météo
- une autre tâche peut être ajoutée pour la suppression des vieilles données et limiter la taille de la base : cleanup_weathermap.php

# Utilisation

Une seule page Web avec les informations météo les plus récentes

<?php

//FONCTION D ALERTE JOURNEE FROIDE

function test_temperature($feels_like,$wind,$wind_deg,$description)
{
    if($feels_like<='7'&&$wind>='15'&&($wind_deg<='33.75'||$wind_deg>='348.75')&&$description<>'ensoleillé')
    {
        return true ;
    } else {
        return false ;
    }
}

//FONCTION DE GENERATION DE L ICONE METEO

function define_weather_icon($id_weather,$night,$db)
{

	$requete = "SELECT icone_jour, icone_nuit FROM wm_icones
					WHERE id = ".$id_weather;
	$resultat = $db->query($requete);

	if (!$resultat)
		echo mysqli_error($db);
	else
	{
		$row = $resultat->fetch_assoc();	
		if($night)
			$icon_name = $row['icone_nuit'];
		else
			$icon_name = $row['icone_jour'];
	}	
	return $icon_name;
} 
    
// FONCTION DE FORMATAGE DE LA DESCRIPTION DU TEMPS

function format_weather_type($description)
{
    if ($description=='partiellement nuageux')
        $description='part. nuageux';
    
    $description=ucfirst($description);
    return $description;
}

// FONCTION DE DIRECTION DU VENT

function direction_vent($angle)
{
    $angle = ($angle % 360) +  ($angle - intval($angle));
     
    $variation = $test = 22.5;
    $list_direction = array('N', 'NE', 'NE', 'E', 'E', 'SE', 'SE', 'S', 'S', 'SO', 'SO', 'O', 'O', 'NO', 'NO', 'N');
 
    for($i=0; $test <= $angle; $test += $variation, $i++)
    {}
    return $list_direction[$i];
}

function direction_vent_precis($angle)
{
    $angle = ($angle % 360) +  ($angle - intval($angle));
    $variation = 22.5;
    $test = 11.25;    
    $list_direction = array('N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSO', 'SO', 'OSO', 'O', 'ONO', 'NO', 'NNO', 'N');
    $list_direction_anglais = array('n', 'nne', 'ne', 'ene', 'e', 'ese', 'se', 'sse', 's', 'ssw', 'sw', 'wsw', 'w', 'wnw', 'nw', 'nnw', 'n');
 
    for($i=0; $test <= $angle; $test += $variation, $i++)
    {}

    return $list_direction_anglais[$i];
}

?>
<!doctype html>
<html lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html" charset="utf-8" />

	<title>Météo</title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" type="text/css" href="css/weather-icons.css" />
	<link rel="stylesheet" type="text/css" href="css/weather-icons-wind.css" />
	<link rel="stylesheet" type="text/css" href="css/minimal.css" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>
<body>
	<br />
	<table>
	<th>
		<a href='#'>
			<svg class="svg-icon" viewBox="0 0 20 20">
			<path d="M18.121,9.88l-7.832-7.836c-0.155-0.158-0.428-0.155-0.584,0L1.842,9.913c-0.262,0.263-0.073,0.705,0.292,0.705h2.069v7.042c0,0.227,0.187,0.414,0.414,0.414h3.725c0.228,0,0.414-0.188,0.414-0.414v-3.313h2.483v3.313c0,0.227,0.187,0.414,0.413,0.414h3.726c0.229,0,0.414-0.188,0.414-0.414v-7.042h2.068h0.004C18.331,10.617,18.389,10.146,18.121,9.88 M14.963,17.245h-2.896v-3.313c0-0.229-0.186-0.415-0.414-0.415H8.342c-0.228,0-0.414,0.187-0.414,0.415v3.313H5.032v-6.628h9.931V17.245z M3.133,9.79l6.864-6.868l6.867,6.868H3.133z"></path>
			</svg>
		</a>
		<a href='./graphique.php'>
			<svg class="svg-icon" viewBox="0 0 20 20">
			<path d="M17.431,2.156h-3.715c-0.228,0-0.413,0.186-0.413,0.413v6.973h-2.89V6.687c0-0.229-0.186-0.413-0.413-0.413H6.285c-0.228,0-0.413,0.184-0.413,0.413v6.388H2.569c-0.227,0-0.413,0.187-0.413,0.413v3.942c0,0.228,0.186,0.413,0.413,0.413h14.862c0.228,0,0.413-0.186,0.413-0.413V2.569C17.844,2.342,17.658,2.156,17.431,2.156 M5.872,17.019h-2.89v-3.117h2.89V17.019zM9.587,17.019h-2.89V7.1h2.89V17.019z M13.303,17.019h-2.89v-6.651h2.89V17.019z M17.019,17.019h-2.891V2.982h2.891V17.019z"></path>
			</svg>
		</a>
	</th>
	</table>
	<br/>
	<table width=100%>
		<tr>
			<th colspan="3"><br/>{$valeurs['loc_name']}<br/>
				<span id="data">{$valeurs['maintenant']}</span><br/><br/>
			</th>
		</tr>
		<tr>
			<td>
         	<b>Relevé de {$valeurs['heure_wm'] }</b><br/>
				<div class="td-centre"><div class="td-gauche">
					<img src="image/temperature.svg">&nbsp;{$valeurs['temp_cur']}°C&nbsp;<br>
					<img src="image/humidite.svg">&nbsp;{$valeurs['humidity_cur']}<span id="unite">%</span>  {$valeurs['dew_point_cur']}<span id="unite">°C</span><br/>
					<div id="vent"><i class="wi wi-wind wi-from-{$valeurs['wind_deg_cur']}"></i><span class="ventc">&nbsp;{$valeurs['wind_speed_cur']}<span id="unite">km/h</span></span></div>
					{if $valeurs['wind_gust_cur'] > 0}<img src="image/rafale.svg">&nbsp;{$valeurs['wind_gust_cur']}<span id="unite">km/h</span><br/>{/if}
					<img src="image/rafale.svg"><img src="image/temperature.svg">&nbsp;{$valeurs['temp_feels_like_cur']}°C
				</div></div>
			</td>
			<td>
				<i class="wi {$valeurs['icon_01']}"></i><br><br>
				<b>{$valeurs['current_time_6']}h</b><br>
				{$valeurs['w_description_01']}
			</td>
			<td>
				<i class="wi {$valeurs['icon_02']}"></i><br><br>
				<b>{$valeurs['current_time_12']}h</b><br>
				{$valeurs['w_description_02']}
			</td>
		</tr>
		<tr>
			<td>
				<b>Aujourd'hui</b><br/>
				{$valeurs['w_description_00']}<br/>
				<div class="td-centre"><div class="td-gauche">                           
					{$valeurs['temp_min_0']}<span id="unite">°C</span> - {$valeurs['temp_max_0']}<span id="unite">°C</span><br/>
					<img src="image/lever.svg">&nbsp;{$valeurs['sunrise_hm']}<br/>
					<img src="image/coucher.svg">&nbsp;{$valeurs['sunset_hm']} 
				</div></div>
			</td>
			<td>
				<br/><br/>
				<div class="td-centre"><div class="td-gauche">
					<img src="image/rafale.svg"><img src="image/temperature.svg">&nbsp;{$valeurs['feels_like_0']}<span id="unite">°C</span><br/>   
					<div id="vent"><i class="wi wi-wind wi-from-{$valeurs['wind_deg_0']}"></i><span class="ventc">&nbsp;{$valeurs['wind_speed_0']}<span id="unite">km/h</span></span></div><br/>
				</div></div>
			</td>
			<td>
				<br/><br/>
				<div class="td-centre"><div class="td-gauche">
					{if $valeurs['rain_0'] > 0 or $valeurs['rain_pop_0'] >0}<img src="image/pluie3.svg">&nbsp;{$valeurs['rain_0']}<span id="unite">mm</span> {$valeurs['rain_pop_0']}<span id="unite">%</span><br/>{/if}
					IUV {$valeurs['uv_0']}<br/><br/>
				</div></div>
			</td>
		</tr>
		<tr id="top">
			<td width=33%>
				<i class="wi {$valeurs['icon_1']}"></i><br/><br/>
				<b>Demain</b> <br/>
				{$valeurs['w_description_1']}<br/>
				<div class="td-centre"><div class="td-gauche">
					{$valeurs['temp_min_1']}<span id="unite">°C</span> - {$valeurs['temp_max_1']}<span id="unite">°C</span><br/>
					<div id="vent"><i class="wi wi-wind wi-from-{$valeurs['wind_deg_1']}"></i><span class="ventc">&nbsp;{$valeurs['wind_speed_1']}<span id="unite">km/h</span></span></div>
					{if $valeurs['rain_1'] > 0 or $valeurs['rain_pop_1'] >0}<img src="image/pluie3.svg">&nbsp;{$valeurs['rain_1']}<span id="unite">mm</span> {$valeurs['rain_pop_1']}<span id="unite">%</span><br/>{/if}
				</div></div>
			</td>
			<td width=33%>
				<i class="wi {$valeurs['icon_2']}"></i><br/><br/>
				<b>{$valeurs['day_2']}</b><br/>
				{$valeurs['w_description_2']}<br/>
				<div class="td-centre"><div class="td-gauche">
					{$valeurs['temp_min_2']}<span id="unite">°C</span> - {$valeurs['temp_max_2']}<span id="unite">°C</span><br/>
					<div id="vent"><i class="wi wi-wind wi-from-{$valeurs['wind_deg_2']}"></i><span class="ventc">&nbsp;{$valeurs['wind_speed_2']}<span id="unite">km/h</span></span></div>
					{if $valeurs['rain_2'] > 0 or $valeurs['rain_pop_2'] >0}<img src="image/pluie3.svg">&nbsp;{$valeurs['rain_2']}<span id="unite">mm</span> {$valeurs['rain_pop_2']}<span id="unite">%</span><br/>{/if}
				</div></div>
			</td>
			<td width=33%>
				<i class="wi {$valeurs['icon_3']}"></i><br/><br/>
				<b>{$valeurs['day_3']}</b><br/>
				{$valeurs['w_description_3']}<br/>
				<div class="td-centre"><div class="td-gauche">
					{$valeurs['temp_min_3']}<span id="unite">°C</span> - {$valeurs['temp_max_3']}<span id="unite">°C</span><br/>
					<div id="vent"><i class="wi wi-wind wi-from-{$valeurs['wind_deg_3']}"></i><span class="ventc">&nbsp;{$valeurs['wind_speed_3']}<span id="unite">km/h</span></span></div>
					{if $valeurs['rain_3'] > 0 or $valeurs['rain_pop_3'] >0}<img src="image/pluie3.svg">&nbsp;{$valeurs['rain_3']}<span id="unite">mm</span> {$valeurs['rain_pop_3']}<span id="unite">%</span><br/>{/if}
				</div></div>
			</td>
		</tr>
	</table>
	<br/>
<br/>
 <iframe frameborder="0" allowTransparency="true" src="https://www.monecowatt.fr/preview-homepage?prevision=1&map=1&ecogestes=1" width="320" height="800"></iframe> 
</body>

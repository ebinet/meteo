<!doctype html>
<html lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html" charset="utf-8" />
	<title>Météo - Graphique</title>
	
	<link rel="stylesheet" type="text/css" href="css/minimal.css" />
	<link rel="stylesheet" type="text/css" href="css/DateTimePicker.css" />

	<script src="js/jquery-3.5.1.min.js"></script> 
	<script src="highcharts/code/highcharts.js"></script>
	<script src="highcharts/code/modules/boost.js"></script>
	<script src="highcharts/code/modules/exporting.js"></script>

	<script src="highcharts/code/modules/series-label.js"></script>
	<script src="highcharts/code/modules/data.js"></script>
	<script src="highcharts/code/modules/export-data.js"></script>

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.13/moment-timezone-with-data-2012-2022.min.js"></script>

<!-- ------------------------------------------------------------>  
<!-- LE SCRIPT HIGHCHARTS SCRIPT.JS EST TOUT EN BAS DE LA PAGE -->
<!-- ------------------------------------------------------------>  
        
</head>

<body>
	<br/>
	<table>
		<th id="hautdepage">
			<span class="fleche" style="float: left;"><a href="#container"><img src="image/flechebas.svg" height="30px" style="margin: 10px 0 0 5px; float: right;" ></a></span>
			<a href='index.php'>
				<svg class="svg-icon" viewBox="0 0 20 20">
				<path d="M18.121,9.88l-7.832-7.836c-0.155-0.158-0.428-0.155-0.584,0L1.842,9.913c-0.262,0.263-0.073,0.705,0.292,0.705h2.069v7.042c0,0.227,0.187,0.414,0.414,0.414h3.725c0.228,0,0.414-0.188,0.414-0.414v-3.313h2.483v3.313c0,0.227,0.187,0.414,0.413,0.414h3.726c0.229,0,0.414-0.188,0.414-0.414v-7.042h2.068h0.004C18.331,10.617,18.389,10.146,18.121,9.88 M14.963,17.245h-2.896v-3.313c0-0.229-0.186-0.415-0.414-0.415H8.342c-0.228,0-0.414,0.187-0.414,0.415v3.313H5.032v-6.628h9.931V17.245z M3.133,9.79l6.864-6.868l6.867,6.868H3.133z"></path>
				</svg>
			</a>
			<a href='#'>                 
				<svg class="svg-icon" viewBox="0 0 20 20">
				<path d="M17.431,2.156h-3.715c-0.228,0-0.413,0.186-0.413,0.413v6.973h-2.89V6.687c0-0.229-0.186-0.413-0.413-0.413H6.285c-0.228,0-0.413,0.184-0.413,0.413v6.388H2.569c-0.227,0-0.413,0.187-0.413,0.413v3.942c0,0.228,0.186,0.413,0.413,0.413h14.862c0.228,0,0.413-0.186,0.413-0.413V2.569C17.844,2.342,17.658,2.156,17.431,2.156 M5.872,17.019h-2.89v-3.117h2.89V17.019zM9.587,17.019h-2.89V7.1h2.89V17.019z M13.303,17.019h-2.89v-6.651h2.89V17.019z M17.019,17.019h-2.891V2.982h2.891V17.019z"></path>
				</svg>
			</a>      
		</th> 
	</table>  

     <div style="margin:10px 0 10px 0" id="data">Données météo : <span id="derniere_donnee"></div></span>&nbsp;<span id="libelle_alerte"></span>

	<table class="graphique">
		<tr>
			<td>
				<div id="container" class="highchart-container"></div>
			</td>
		</tr>
	</table>
	

	<div class="wrapper">
		<div class="table2 grille-controles">
			<div class="td2" width=10% style="vertical-align: top; text-align: center;" rowspan="2">
				<input class="button-css" type="button" value="OK" onclick="updateChart(); UpdateTitle();"><br/><br>
				<input class="button-css" type="button" value="RAZ" onclick="init_saisie(); updateChart(); UpdateTitle();"><br>
				<span class="fleche"><a href="#container"><img src="image/flechehaut.svg" height="30px" style="margin: 10px 0 0 5px;" ></a></span>
			</div>
			<div class="td2">
				<div class="puces-dates">
					<div>
						<input type="radio" id="duree" name="select_donnees" value="duree" {$select_duree}>
					</div>
					<div>
						Depuis <input type="text" inputmode="decimal" size="2" id="nb_heures" name="nb_heures" value="{$nb_heures}" onchange='$("#duree").prop("checked", true);'> heures<br/>
						<br/>
					</div>
				</div>
				<div class="puces-dates">
					<div>
						<input type="radio" id="intervalle" name="select_donnees" value="intervale" {$select_intervale}>
					</div>
					<div>
						Du <input type="date" id="date_debut" name="date_debut" onchange='$("#intervalle").prop("checked", true);'>
						<input type="time" id="heure_debut" name="heure_debut" onchange='$("#intervalle").prop("checked", true);'>
						<br/><br/>                   
						au <input type="date" id="date_fin" name="date_fin" data-field="datetime" onchange='$("#intervalle").prop("checked", true);'>
						<input type="time" id="heure_fin" name="heure_fin" data-field="datetime" onchange='$("#intervalle").prop("checked", true);'>
					</div>
				</div>
			</div>
		</div>
		<div class="table2 grille-affichage">
			<div class="td2">
				<input type="checkbox" id="check_temp_cur" name="check_temp_cur" onclick="clicbox(this.id)" {$liste_courbes['check_temp_cur']} >
				<label for="check_temp_cur">Température</label><br/>

				<input type="checkbox" id="check_temp_feels_like_cur" name="check_temp_feels_like_cur" onclick="clicbox(this.id)" {$liste_courbes['check_temp_feels_like_cur']}>
				<label for="check_temp_feels_like_cur">Température ressentie</label><br/>

				<input type="checkbox" id="check_humidity_cur" name="check_humidity_cur" onclick="clicbox(this.id)" {$liste_courbes['check_humidity_cur']} >
				<label for="check_humidity_cur">Humidité</label><br/>

				<input type="checkbox" id="check_dew_point_cur" name="check_dew_point_cur" onclick="clicbox(this.id)" {$liste_courbes['check_dew_point_cur']} >
				<label for="check_dew_point_cur">Point de rosée</label><br/>

			</div>
			<div class="td2">
				<input type="checkbox" id="check_wind_deg_cur" name="check_wind_deg_cur" onclick="clicbox(this.id)" {$liste_courbes['check_wind_deg_cur']}>
				<label for="check_wind_deg_cur">Direction du vent</label><br/>

				<input type="checkbox" id="check_wind_speed_cur" name="check_wind_speed_cur" onclick="clicbox(this.id)" {$liste_courbes['check_wind_speed_cur']}>
				<label for="check_wind_speed_cur">Vitesse du vent</label><br/>

				<input type="checkbox" id="check_wind_gust_cur" name="check_wind_gust_cur" onclick="clicbox(this.id)" {$liste_courbes['check_wind_gust_cur']} >
				<label for="check_wind_gust_cur">Rafales de vent</label><br/>
			</div>
			<div class="td2">
				<input type="checkbox" id="grouper" name="grouper" onclick="clicbox_grouper()">
				<label for="grouper">Regrouper</label><br/>
			</div>
			<div class="td2">
				<input class="button-css" type="button" value="RAZ" onclick="verifie_series()">
			</div>
		</div>
	</div>


	<br/>

<script type="text/javascript" >
	{include file='js/script.js'}
</script> 


</body>
    
</html>
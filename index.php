<?php
 
include("includes/smarty.inc.php"); 
include("includes/config.inc.php"); 
include("includes/fonctions.inc.php"); 

$valeurs = array();

$db = new mysqli($db_host, $db_user, $db_pwd, $db_name);

// Extract Weathermap data 

    $requete = "SELECT * FROM weathermap
					WHERE timestamp = (SELECT MAX(timestamp) FROM weathermap)";

	$results = $db->query($requete);

$res = mysqli_fetch_assoc($results);

    $valeurs['temp_cur']=str_replace('.',',',$res['temp_cur']);
    $valeurs['temp_feels_like_cur']=str_replace('.',',',$res['temp_feels_like_cur']);
    $valeurs['humidity_cur']=$res['humidity_cur'];
    $valeurs['dew_point_cur']=str_replace('.',',',$res['dew_point_cur']);
    $valeurs['wind_deg_cur']=direction_vent_precis($res['wind_deg_cur']);
    $valeurs['wind_speed_cur']=$res['wind_speed_cur'];
    $valeurs['wind_gust_cur']=$res['wind_gust_cur'];

    $valeurs['temp_min_0']=$res['min_0'];
    $valeurs['temp_max_0']=$res['max_0'];
    $valeurs['humidity_0']=$res['humidity_0'];
    $valeurs['wind_speed_0']=$res['wind_0'];
    $valeurs['wind_deg_0']=direction_vent_precis($res['wind_deg_0']);
    $valeurs['w_id_00']=$res['id_00'];
    $valeurs['w_description_00']=$res['description_00'];
    $valeurs['current_time_6']=$res['dt_01'];
    $valeurs['w_id_01']=$res['id_01'];
    $valeurs['w_description_01']=$res['description_01'];
    $valeurs['current_time_12']=$res['dt_02'];
    $valeurs['w_id_02']=$res['id_02'];
    $valeurs['w_description_02']=$res['description_02'];
    $valeurs['rain_0']=$res['rain_0'];
    $valeurs['rain_pop_0']=$res['rain_pop_0'];
    $valeurs['uv_0']=$res['uv_0'];
    $valeurs['dt_0']=$res['dt_0'];  
    $valeurs['sunrise_0']=$res['sunrise_0'];  
    $valeurs['sunset_0']=$res['sunset_0'];  

    $valeurs['temp_min_1']=$res['min_1'];
    $valeurs['temp_max_1']=$res['max_1'];
    $valeurs['humidity_1']=$res['humidity_1'];
    $valeurs['wind_speed_1']=$res['wind_1'];
    $valeurs['wind_deg_1']=direction_vent_precis($res['wind_deg_1']);
    $valeurs['w_id_1']=$res['id_1'];
    $valeurs['w_description_1']=$res['description_1'];
    $valeurs['rain_1']=$res['rain_1'];
    $valeurs['rain_pop_1']=$res['rain_pop_1'];
    $valeurs['dt_1']=$res['dt_1']; 

    $valeurs['temp_min_2']=$res['min_2'];
    $valeurs['temp_max_2']=$res['max_2'];
    $valeurs['humidity_2']=$res['humidity_2'];
    $valeurs['wind_speed_2']=$res['wind_2'];
    $valeurs['wind_deg_2']=direction_vent_precis($res['wind_deg_2']);
    $valeurs['w_id_2']=$res['id_2'];
    $valeurs['w_description_2']=$res['description_2'];
    $valeurs['rain_2']=$res['rain_2'];
    $valeurs['rain_pop_2']=$res['rain_pop_2'];
    $valeurs['dt_2']=$res['dt_2']; 

    $valeurs['temp_min_3']=$res['min_3'];
    $valeurs['temp_max_3']=$res['max_3'];
    $valeurs['humidity_3']=$res['humidity_3'];
    $valeurs['wind_speed_3']=$res['wind_3'];
    $valeurs['wind_deg_3']=direction_vent_precis($res['wind_deg_3']);
    $valeurs['w_id_3']=$res['id_3'];
    $valeurs['w_description_3']=$res['description_3'];
    $valeurs['rain_3']=$res['rain_3'];
    $valeurs['rain_pop_3']=$res['rain_pop_3'];
    $valeurs['dt_3']=$res['dt_3']; 

    $valeurs['feels_like_0']=$res['feels_like_0'];
    $valeurs['timestamp_wm']=$res['timestamp'];

    $valeurs['loc_name'] = $loc_name;

// Prochain lever de soleil

$valeurs['next_sunrise']=$valeurs['sunrise_0']+86400;

// Création des icones meteo tenant compte de la nuit ou du jour 

$night=false;
if ($valeurs['timestamp_wm']<$valeurs['sunrise_0'] OR ($valeurs['timestamp_wm']>$valeurs['sunset_0'] && $valeurs['timestamp_wm']<$valeurs['next_sunrise']))
        $night=true;
$valeurs['icon_00']=define_weather_icon($valeurs['w_id_00'],$night,$db);

$night=false;
if ($valeurs['current_time_6']<$valeurs['sunrise_0'] OR ($valeurs['current_time_6']>$valeurs['sunset_0']&& $valeurs['current_time_6']<$valeurs['next_sunrise']))
        $night=true;
$valeurs['icon_01']=define_weather_icon($valeurs['w_id_01'],$night,$db);

$night=false;
if ($valeurs['current_time_12']<$valeurs['sunrise_0'] OR ($valeurs['current_time_12']>$valeurs['sunset_0']&& $valeurs['current_time_12']<$valeurs['next_sunrise']))
        $night=true;
$valeurs['icon_02']=define_weather_icon($valeurs['w_id_02'],$night,$db);

$night=false;
$valeurs['icon_1']=define_weather_icon($valeurs['w_id_1'],$night,$db);
$valeurs['icon_2']=define_weather_icon($valeurs['w_id_2'],$night,$db);
$valeurs['icon_3']=define_weather_icon($valeurs['w_id_3'],$night,$db);

// Est-ce que les demi-journées sont pour aujourd'hui ou demain ?

$minuit = strtotime("today 23:59");
if ($valeurs['current_time_6'] > $minuit)
	$jour_6 = "Demain";
else
	$jour_6 = "";
if ($valeurs['current_time_12'] > $minuit)
	$jour_12 = "Demain";
else
	$jour_12 = "";
	
// Formatage des heures au format hh:mm:ss

$valeurs['sunrise_hm']= date("H:i", $valeurs['sunrise_0']);
$valeurs['sunset_hm']= date("H:i", $valeurs['sunset_0']);
$valeurs['sunrise_0']= date("H:i:s", $valeurs['sunrise_0']);
$valeurs['sunset_0']= date("H:i:s", $valeurs['sunset_0']);
$valeurs['current_time_6']= $jour_6." ".date("H", $valeurs['current_time_6']);
$valeurs['current_time_12']= $jour_12." ".date("H", $valeurs['current_time_12']);
$valeurs['current_time']=date("H:i:s", $valeurs['timestamp_wm']);

// Encodage du nom des jours suivants 

$valeurs['timestamp'] = date("d/m/Y H:i:s");
$valeurs['heure_wm'] = date("H:i", $valeurs['timestamp_wm']);
$valeurs['timestamp_wm'] = date("d/m/Y H:i:s", $valeurs['timestamp_wm']);

$valeurs['maintenant']=ucwords(strftime('%A %e %B %Y'));

$valeurs['day_0']=ucfirst(strftime("%A",$valeurs['dt_0']));
$valeurs['day_1']=ucfirst(strftime("%A",$valeurs['dt_1']));
$valeurs['day_2']=ucfirst(strftime("%A",$valeurs['dt_2']));
$valeurs['day_3']=ucfirst(strftime("%A",$valeurs['dt_3']));


// Formatage description temps

$valeurs['w_description_00']=format_weather_type($valeurs['w_description_00']);
$valeurs['w_description_01']=format_weather_type($valeurs['w_description_01']);
$valeurs['w_description_02']=format_weather_type($valeurs['w_description_02']);
$valeurs['w_description_1']=format_weather_type($valeurs['w_description_1']);
$valeurs['w_description_2']=format_weather_type($valeurs['w_description_2']);
$valeurs['w_description_3']=format_weather_type($valeurs['w_description_3']);

// Envoi du template

$tpl->assign("valeurs",$valeurs);
//$tpl->display("visualisation.tpl");
$tpl->display("visualisation-min.tpl");

?>
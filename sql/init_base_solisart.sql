-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  localhost
-- Généré le :  Dim 22 Novembre 2020 à 16:58
-- Version du serveur :  10.3.25-MariaDB-0+deb10u1
-- Version de PHP :  7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Structure de la table `weathermap`
--

CREATE TABLE `weathermap` (
  `timestamp` int(12) NOT NULL,
  `temp_cur` decimal(4,1) DEFAULT NULL,
  `temp_feels_like_cur` decimal(4,1) DEFAULT NULL,
  `humidity_cur` decimal(4,0) DEFAULT NULL,
  `dew_point_cur` decimal(4,1) DEFAULT NULL,
  `wind_deg_cur` decimal(10,0) NOT NULL,
  `wind_speed_cur` decimal(4,0) DEFAULT NULL,
  `wind_gust_cur` decimal(4,0) DEFAULT NULL,
  `dt_0` int(12) NOT NULL,
  `sunrise_0` int(12) NOT NULL,
  `sunset_0` int(12) NOT NULL,
  `id_00` int(4) NOT NULL,
  `description_00` varchar(30) NOT NULL,
  `dt_01` int(12) NOT NULL,
  `id_01` int(4) NOT NULL,
  `description_01` varchar(30) DEFAULT NULL,
  `dt_02` int(12) NOT NULL,
  `id_02` int(4) NOT NULL,
  `description_02` varchar(30) DEFAULT NULL,
  `min_0` decimal(4,0) DEFAULT NULL,
  `max_0` decimal(4,0) DEFAULT NULL,
  `feels_like_0` decimal(4,0) NOT NULL,
  `humidity_0` decimal(4,0) DEFAULT NULL,
  `wind_0` decimal(4,0) DEFAULT NULL,
  `wind_deg_0` decimal(10,0) NOT NULL,
  `rain_0` decimal(4,0) DEFAULT NULL,
  `rain_pop_0` decimal(4,0) DEFAULT NULL,
  `uv_0` decimal(4,0) NOT NULL,
  `dt_1` int(12) NOT NULL,
  `id_1` int(4) NOT NULL,
  `description_1` varchar(30) NOT NULL,
  `min_1` decimal(4,0) NOT NULL,
  `max_1` decimal(4,0) NOT NULL,
  `humidity_1` decimal(4,0) NOT NULL,
  `wind_1` decimal(4,0) NOT NULL,
  `wind_deg_1` decimal(10,0) NOT NULL,
  `rain_1` decimal(4,0) NOT NULL,
  `rain_pop_1` decimal(4,0) DEFAULT NULL,
  `dt_2` int(12) NOT NULL,
  `id_2` int(4) NOT NULL,
  `description_2` varchar(30) NOT NULL,
  `min_2` decimal(4,0) NOT NULL,
  `max_2` decimal(4,0) NOT NULL,
  `humidity_2` decimal(4,0) NOT NULL,
  `wind_2` decimal(4,0) NOT NULL,
  `wind_deg_2` decimal(10,0) NOT NULL,
  `rain_2` decimal(4,0) NOT NULL,
  `rain_pop_2` decimal(4,0) DEFAULT NULL,
  `dt_3` int(12) NOT NULL,
  `id_3` int(4) NOT NULL,
  `description_3` varchar(30) NOT NULL,
  `min_3` decimal(4,0) NOT NULL,
  `max_3` decimal(4,0) NOT NULL,
  `humidity_3` decimal(4,0) NOT NULL,
  `wind_3` decimal(4,0) NOT NULL,
  `wind_deg_3` decimal(10,0) NOT NULL,
  `rain_3` decimal(4,0) NOT NULL,
  `rain_pop_3` decimal(4,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour la table `weathermap`
--
ALTER TABLE `weathermap`
  ADD PRIMARY KEY (`timestamp`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

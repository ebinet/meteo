<?php

include("includes/config.inc.php"); 

$db = new mysqli($db_host, $db_user, $db_pwd, $db_name);

// Fonction de conversion à zéro si null
function convnull($var)
{
	if (is_null($var))
		return 0;
	else
		return $var;
}

// Recherche des demi-journées à récupérer

$heure_actuelle = (int)(date("H"));

if ($heure_actuelle < 10)
{
	$jour1 = "today";
	$heure1 = 11;
	$jour2 = "today";
	$heure2 = 15;
}
else
	if ($heure_actuelle < 14)
	{
		$jour1 = "today";
		$heure1 = 15;
		$jour2 = "tomorrow";
		$heure2 = 11;
	}
	else
	{
		$jour1 = "tomorrow";
		$heure1 = 11;
		$jour2 = "tomorrow";
		$heure2 = 15;		
	}

//Calcul des occurences à récupérer
$date1 = strtotime("$jour1 $heure1:00");
$date2 = strtotime("$jour2 $heure2:00");
$resultat1 = date("d/m/Y H:i:s",$date1);
$resultat2 = date("d/m/Y H:i:s",$date2);

// Takes raw data from the request

$url='https://api.openweathermap.org/data/3.0/onecall?lat='.$lat.'&lon='.$lon.'&exclude=minutely,alerts&appid='.$api_key.'&units=metric&lang=fr';

//echo $url;die;

$json = file_get_contents($url);

// debug json file
//die($json);
//$json = file_get_contents('http://192.168.1.2/onecall.json');


// Converts it into a an array
$data = json_decode($json,true);

$temp_cur = round($data['current']['temp'],1);
$temp_feels_like_cur = round($data['current']['feels_like'],1);
$humidity_cur = round($data['current']['humidity'],0);
$dew_point_cur = round($data['current']['dew_point'],1);
$wind_deg_cur = round($data['current']['wind_deg'],0);
$wind_speed_cur=round($data['current']['wind_speed']*60*60/1000,0);
$wind_gust_cur=round(convnull($data['current']['wind_gust']*60*60/1000,0));

$dt_0=$data['daily'][0]['dt'];
$sunrise_0=$data['daily'][0]['sunrise'];
$sunset_0=$data['daily'][0]['sunset'];
$temp_min_0=round($data['daily'][0]['temp']['min'],0);
$temp_max_0=round($data['daily'][0]['temp']['max'],0);
$temp_feels_like_0=round($data['daily'][0]['feels_like']['day'],0);
$wind_deg_0=round($data['daily'][0]['wind_deg'],0);
$humidity_0=round($data['daily'][0]['humidity'],0);
$wind_speed_0=round($data['daily'][0]['wind_speed']*60*60/1000,0);
$w_id_00=$data['hourly'][0]['weather'][0]['id'];
$w_description_00=$data['hourly'][0]['weather'][0]['description'];
$rain_0=round(convnull($data['daily'][0]['rain'],0));
$rain_pop_0=round($data['daily'][0]['pop']*100,0);
$uv_0=round($data['daily'][0]['uvi'],0);

$dt_1=$data['daily'][1]['dt'];
$temp_min_1=round($data['daily'][1]['temp']['min'],0);
$temp_max_1=round($data['daily'][1]['temp']['max'],0);
$humidity_1=round($data['daily'][1]['humidity'],0);
$wind_speed_1=round($data['daily'][1]['wind_speed']*60*60/1000,0);
$wind_deg_1=round($data['daily'][1]['wind_deg'],0);
$w_id_1=$data['daily'][1]['weather'][0]['id'];
$w_description_1=$data['daily'][1]['weather'][0]['description'];
$rain_1=round(convnull($data['daily'][1]['rain'],0));
$rain_pop_1=round($data['daily'][1]['pop']*100,0);

$dt_2=$data['daily'][2]['dt'];
$temp_min_2=round($data['daily'][2]['temp']['min'],0);
$temp_max_2=round($data['daily'][2]['temp']['max'],0);
$humidity_2=round($data['daily'][2]['humidity'],0);
$wind_speed_2=round($data['daily'][2]['wind_speed']*60*60/1000,0);
$wind_deg_2=round($data['daily'][2]['wind_deg'],0);
$w_id_2=$data['daily'][2]['weather'][0]['id'];
$w_description_2=$data['daily'][2]['weather'][0]['description'];
$rain_2=round(convnull($data['daily'][2]['rain'],0));
$rain_pop_2=round($data['daily'][2]['pop']*100,0);

$dt_3=$data['daily'][3]['dt'];
$temp_min_3=round($data['daily'][3]['temp']['min'],0);
$temp_max_3=round($data['daily'][3]['temp']['max'],0);
$humidity_3=round($data['daily'][3]['humidity'],0);
$wind_speed_3=round($data['daily'][3]['wind_speed']*60*60/1000,0);
$wind_deg_3=round($data['daily'][3]['wind_deg'],0);
$w_id_3=$data['daily'][3]['weather'][0]['id'];
$w_description_3=$data['daily'][3]['weather'][0]['description'];
$rain_3=round(convnull($data['daily'][3]['rain'],0));
$rain_pop_3=round($data['daily'][3]['pop']*100,0);

$timestamp=time();

// Affectation des demi-journées

foreach($data['hourly'] as $key => $value)
{
	if ($value['dt']==$date1)
	{
		$w_id_01 = $data['hourly'][$key]['weather'][0]['id'];
		$w_description_01 = $data['hourly'][$key]['weather'][0]['description'];
	}
	if ($value['dt']==$date2)
	{
		$w_id_02 = $data['hourly'][$key]['weather'][0]['id'];
		$w_description_02 = $data['hourly'][$key]['weather'][0]['description'];
	}
}


// On vérifie que le timestamp n'existe pas déjà en base
	
	$requete_verif = "SELECT timestamp FROM weathermap WHERE timestamp = ".$timestamp;
    //echo($requete_verif);
	$result = $db->query($requete_verif);
	if (!$result)
		echo mysqli_error($db);
	else
	{
		$row = $result->fetch_assoc();
	
		if ($row['timestamp'] != $timestamp)
		{
	
			// Insertion des données dans la base mysql
            
			$requete = "INSERT INTO weathermap (timestamp,temp_cur,temp_feels_like_cur,humidity_cur,dew_point_cur,wind_deg_cur,wind_speed_cur,wind_gust_cur,dt_0,sunrise_0,sunset_0,id_00,description_00,dt_01,id_01,description_01,dt_02,id_02,description_02,min_0,max_0,feels_like_0,humidity_0,wind_0,wind_deg_0,rain_0,rain_pop_0,uv_0,dt_1,id_1,description_1,min_1,max_1,humidity_1,wind_1,wind_deg_1,rain_1,rain_pop_1,dt_2,id_2,description_2,min_2,max_2,humidity_2,wind_2,wind_deg_2,rain_2,rain_pop_2,dt_3,id_3,description_3,min_3,max_3,humidity_3,wind_3, wind_deg_3,rain_3,rain_pop_3)
				VALUES ('$timestamp','$temp_cur','$temp_feels_like_cur','$humidity_cur','$dew_point_cur','$wind_deg_cur','$wind_speed_cur','$wind_gust_cur','$dt_0','$sunrise_0','$sunset_0',$w_id_00,'$w_description_00',$date1,$w_id_01,'$w_description_01',$date2,$w_id_02,'$w_description_02','$temp_min_0','$temp_max_0','$temp_feels_like_0','$humidity_0','$wind_speed_0','$wind_deg_0','$rain_0','$rain_pop_0','$uv_0','$dt_1',$w_id_1,'$w_description_1','$temp_min_1','$temp_max_1','$humidity_1','$wind_speed_1','$wind_deg_1','$rain_1','$rain_pop_1','$dt_2',$w_id_2,'$w_description_2','$temp_min_2','$temp_max_2','$humidity_2','$wind_speed_2','$wind_deg_2','$rain_2','$rain_pop_2','$dt_3',$w_id_3,'$w_description_3','$temp_min_3','$temp_max_3','$humidity_3','$wind_speed_3','$wind_deg_3','$rain_3','$rain_pop_3')";
        
			 // echo $requete."<br />";
		
			if (!$db->query($requete))
				echo mysqli_error($db);
		
		}
	}


?>
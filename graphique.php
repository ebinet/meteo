<?php

include("includes/smarty.inc.php");
include("includes/config.inc.php");

// --->  Tout l'ancien code est désormais dans le script javascript <---
// On pourrait se passer complètement du PHP mais je le garde pour faciliter les tests avec l'URL dynamique


// Construction des URL de récupération des données par Ajax
$url_data_sans_parametre = $url_site."/data_meteo.php";
$url_derniere_donnee = $url_site."/derniere_donnee.php";

$tpl->assign("url_data_sans_parametre",$url_data_sans_parametre);
$tpl->assign("url_derniere_donnee",$url_derniere_donnee);


// Envoi du template

$tpl->display("graphique-min.tpl");


?>
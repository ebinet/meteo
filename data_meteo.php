<?php

include("includes/config.inc.php"); 

if ($_GET['type'])
	$type=$_GET['type'];
else
	$type="duree";

if ($type != 'duree' and $type != 'intervale')
	die("Erreur de type : ".$type);	

// Type durée
if ($type=='duree')
{
	if ($_GET['nb_heures'])
		$nb_heures=$_GET['nb_heures'];
	else
		$nb_heures=48;

	if (!is_numeric($nb_heures))
		die("Erreur de format du nombre d'heures");	

	$debut = strtotime("-$nb_heures hour", time());	
	
	$condition_requete = " WHERE timestamp > ".$debut; 
}

// Type intervale
if ($type=='intervale')
{
	if ($_GET['debut'])
		$debut=$_GET['debut'];
	if (!is_numeric($debut))
		die("Erreur de format du timestamp de début : ".$debut);	

	if ($_GET['fin'])
		$fin=$_GET['fin'];
	if (!is_numeric($fin))
		die("Erreur de format du timestamp de fin : ".$fin);	
	
	$condition_requete = " WHERE timestamp >= ".$debut." AND timestamp <= ".$fin; 
}


$db = new mysqli($db_host, $db_user, $db_pwd, $db_name);

	$requete = "SELECT timestamp, temp_cur, temp_feels_like_cur, humidity_cur, dew_point_cur, wind_deg_cur, wind_speed_cur, wind_gust_cur FROM weathermap ".$condition_requete;
	$results = $db->query($requete);

	header("Content-Type: application/csv-tab-delimited-table"); 
	header("Content-disposition: filename=data.csv"); 
	
	//En-têtes de colonnes
	echo "timestamp, Température, Température ressentie, Humidité, Point de rosée, Direction du vent, Vitesse du vent, Rafales de vent\n";

	while($row = $results->fetch_assoc())
	{
	    $timestamp3 = $row['timestamp']*1000; // millisecondes
	    echo($timestamp3.",");
	    echo($row['temp_cur'].",");
	    echo($row['temp_feels_like_cur'].",");
	    echo($row['humidity_cur'].",");
	    echo($row['dew_point_cur'].",");
	    echo($row['wind_deg_cur'].",");
	    echo($row['wind_speed_cur'].",");
	    echo($row['wind_gust_cur']."\n");
	}
	
// var_dump($tableau);
//	echo(json_encode($tableau));


?>